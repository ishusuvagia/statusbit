//
//  StatusBitFBHeader.h
//  StatusBit
//
//  Created by iSquare infoTech on 4/5/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

#ifndef StatusBitFBHeader_h
#define StatusBitFBHeader_h

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

#endif /* StatusBitFBHeader_h */
