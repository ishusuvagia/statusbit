//
//  CustomTabBarVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/3/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit


class CustomTabBarVC: UIViewController {
    
    let app = (UIApplication.shared.delegate as? AppDelegate)
    
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnMyDCN: UIButton!
    @IBOutlet weak var btnStatusBits: UIButton!
    @IBOutlet weak var btnAlert: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnStatusPressed(_ sender: Any) {
//        if btnStatus.isSelected == true {
//            btnStatus.isSelected = false
//        }
//        else {
            btnStatus.isSelected = true
            
            let navigationController: UINavigationController? = (app?.window?.rootViewController as? UINavigationController)
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileView: HomeVC? = (mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC)
            navigationController?.pushViewController(profileView!, animated: false)
//        }
    }
    
    @IBAction func btnMyDCNPressed(_ sender: Any) {
//        if btnMyDCN.isSelected == true {
//            btnMyDCN.isSelected = false
//        }
//        else {
            btnMyDCN.isSelected = true

            let navigationController: UINavigationController? = (app?.window?.rootViewController as? UINavigationController)
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileView: MyDCN_VC? = (mainStoryboard.instantiateViewController(withIdentifier: "MyDCN_VC") as? MyDCN_VC)
            navigationController?.pushViewController(profileView!, animated: false)
//        }
    }

    @IBAction func btnStatusBitsPressed(_ sender: Any) {
        if btnStatusBits.isSelected == true {
            btnStatusBits.isSelected = false
        }
        else {
            btnStatusBits.isSelected = true
            
            

        }
    }
    
    @IBAction func btnAlertsPressed(_ sender: Any) {
        if btnAlert.isSelected == true {
            btnAlert.isSelected = false
        }
        else {
            btnAlert.isSelected = true
            
            let navigationController: UINavigationController? = (app?.window?.rootViewController as? UINavigationController)
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileView: AlertVC? = (mainStoryboard.instantiateViewController(withIdentifier: "AlertVC") as? AlertVC)
            navigationController?.pushViewController(profileView!, animated: false)
        }
    }
    
}
