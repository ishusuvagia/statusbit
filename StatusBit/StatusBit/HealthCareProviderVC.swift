//
//  HealthCareProviderVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 5/2/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class HealthCareProviderVC: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    var pageNo:NSInteger = 0
    var returnKeyHandler: IQKeyboardReturnKeyHandler!
    @IBOutlet weak var lblHeader: UILabel!
    let picker = UIImagePickerController()

//    First View
    
    @IBOutlet weak var txtFConfPSW,txtFPSW,txtFPhoneNum,txtFEmail: VMFloatLabelTextField!
    @IBOutlet weak var lblNext: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    
//    Second View

    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var txtFGetNpi: VMFloatLabelTextField!
    @IBOutlet weak var txtFNameOfPract: VMFloatLabelTextField!
    
//    Third View
    
    @IBOutlet weak var thirdView: UIView!
    
    @IBOutlet weak var lblThirdUserName: UILabel!
    @IBOutlet weak var imgThirdProfile: UIImageView!
    
    
    
//    MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        picker.delegate = self
        
        startDesign()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startDesign() {
        
        secondView.isHidden = true
        thirdView.isHidden = true
        
        bottomBorder(select: 0, txtField: txtFConfPSW)
        bottomBorder(select: 0, txtField: txtFPSW)
        bottomBorder(select: 0, txtField: txtFEmail)
        bottomBorder(select: 0, txtField: txtFPhoneNum)
        bottomBorder(select: 0, txtField: txtFGetNpi)
        bottomBorder(select: 0, txtField: txtFNameOfPract)
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width/2
        imgUserProfile.layer.masksToBounds = true
        
    }
    
    func bottomBorder(select:NSInteger, txtField:UITextField) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.orange.cgColor
        }
        else {
            border.borderColor = UIColor.lightGray.cgColor
        }
        
        border.frame = CGRect(x: 0, y: txtField.frame.size.height - width, width:  txtField.frame.size.width, height: txtField.frame.size.height)
        
        border.borderWidth = width
        txtField.layer.addSublayer(border)
        txtField.layer.masksToBounds = true
    }
    
    func showAlert(msg:String) {
        let  alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        
        let okAcction = UIAlertAction(title: "OK", style: .cancel) { (UIAlertAction) in
            
        }
        
        alert.addAction(okAcction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
//    MARK: - Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.becomeFirstResponder()
        bottomBorder(select: 1, txtField: textField)
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    func selectoption(opt:NSInteger) {
        picker.allowsEditing = false
        if opt == 1 {
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        else {
            picker.sourceType = .camera
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - Delegates
    private func imagePickerController(_ picker: UIImagePickerController,
                                       didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        imgUserProfile.contentMode = .scaleAspectFit //3
        imgUserProfile.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //        imgUserProfile.contentMode = .scaleAspectFit //3
        imgUserProfile.image = chosenImage //4
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width/2
        imgUserProfile.layer.masksToBounds = true
        dismiss(animated:true, completion: nil) //5
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    
//    MARK: - Action Methods
    
    @IBAction func btnNextPressed(_ sender: Any) {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if pageNo == 0 {
            if txtFPSW.text?.characters.count == 0 || txtFEmail.text?.characters.count == 0 || txtFConfPSW.text?.characters.count == 0 ||   txtFPhoneNum.text?.characters.count == 0 {
                showAlert(msg: "Please enter all fields")
            }
            else if !(emailTest.evaluate(with: txtFEmail?.text)) {
                showAlert(msg: "Please enter valid email")
            }
            else if !(txtFPSW?.text == txtFConfPSW?.text) {
                showAlert(msg: "Password mismatch")
            }
            else {
                pageNo = 1
                secondView.isHidden = false
                thirdView.isHidden = true
                
                
            }
            
        }
        else if pageNo == 1 {
            if txtFNameOfPract.text?.characters.count == 0 || txtFGetNpi.text?.characters.count == 0  {
                showAlert(msg: "Please enter all fields")
            }
            else{
                pageNo = 2
                lblHeader.text = "Almost Done"
                lblNext.text = "VERIFY"
                secondView.isHidden = true
                thirdView.isHidden = false
                
                
            }
        }
        else {
            
            lblHeader.text = "Healthcare Provider Setup"
            lblNext.text = "NEXT"
            
            pageNo = 0
            secondView.isHidden = true
            thirdView.isHidden = true
            
            txtFPSW.text = nil
            txtFEmail.text = nil
            txtFConfPSW.text = nil
            txtFPhoneNum.text = nil
            txtFGetNpi.text = nil
            txtFNameOfPract.text = nil
            
            let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            navigationController?.pushViewController(hvc, animated: true)
            
        }
    }
    
    @IBAction func btnChangeProfileImg(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image From", message: nil, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            self.selectoption(opt: 0)
        }
        
        let Gallery = UIAlertAction(title: "Gallery", style: .default) { (UIAlertAction) in
            self.selectoption(opt: 1)
        }
        
        let Cancel = UIAlertAction(title: "Cancel", style: .default) { (UIAlertAction) in
            
        }
        
        alert.addAction(camera)
        alert.addAction(Gallery)
        alert.addAction(Cancel)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnGoBackPressed(_ sender: Any) {
        if pageNo == 0 {
            _ = navigationController?.popViewController(animated: true)
        }
        else if pageNo == 1 {
            pageNo = 0
            secondView.isHidden = true
            thirdView.isHidden = true
        }
        else {
            pageNo = 1
            lblHeader.text = "Healthcare Provider Setup"
            lblNext.text = "NEXT"
            secondView.isHidden = false
            thirdView.isHidden = true
        }
        
    }
    
    @IBAction func btnGetNPIPressed(_ sender: Any) {
        
    }
    
}
