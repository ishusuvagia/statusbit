//
//  RequestVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 5/3/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class RequestVC: UIViewController {
    
    
    @IBOutlet weak var lblHeader: UILabel!
    
//    Email View
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var lblShowConfirmEmailPhone: UILabel!
    @IBOutlet weak var txtFGetPhoneEmail: UITextField!
    

//    Phone View
    
//    MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startDesign()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startDesign() {
        emailView.isHidden = true
        bottomBorder(select: 0, txtField: txtFGetPhoneEmail)
        
        if #available(iOS 9.0, *) {
            let defaultClearButton = UIButton.appearance(whenContainedInInstancesOf: [UITextField.self])
             defaultClearButton.setBackgroundImage(UIImage(named: "Chatcancel-btn.png"), for: UIControlState.normal)
        } else {
            // Fallback on earlier versions
        }
        
        
       
        
    }
    
    func bottomBorder(select:NSInteger, txtField:UITextField) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.orange.cgColor
        }
        else {
            border.borderColor = UIColor.lightGray.cgColor
        }
        
        border.frame = CGRect(x: 0, y: txtField.frame.size.height - width, width:  txtField.frame.size.width, height: txtField.frame.size.height)
        
        border.borderWidth = width
        txtField.layer.addSublayer(border)
        txtField.layer.masksToBounds = true
    }
    
//    MARK: - Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.becomeFirstResponder()
        bottomBorder(select: 1, txtField: textField)
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
//    MARK: - Action Methods

    @IBAction func btnMorePressed(_ sender: Any) {
        
    }
    
    @IBAction func btnFBContectPressed(_ sender: Any) {
        
    }
    
    @IBAction func btnEmailPressed(_ sender: Any) {
        emailView.isHidden = false
        txtFGetPhoneEmail.text = nil
        lblShowConfirmEmailPhone.text = "Is this a correct email address ?"
        lblHeader.text = "Confirm Email"
        txtFGetPhoneEmail.placeholder = "Enter email address"
        txtFGetPhoneEmail.keyboardType = .emailAddress
    }
    
    @IBAction func btnContactPressed(_ sender: Any) {
        emailView.isHidden = false
        txtFGetPhoneEmail.text = nil
        lblShowConfirmEmailPhone.text = "Is this a correct phone number ?"
        lblHeader.text = "Confirm Phone Number"
        txtFGetPhoneEmail.placeholder = "Enter phone number"
        txtFGetPhoneEmail.keyboardType = .phonePad
    }
    
    @IBAction func btnGoBack(_ sender: Any) {
        if emailView.isHidden == false {
            emailView.isHidden = true
        }
        else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        
    }

    @IBAction func btnSendPressed(_ sender: Any) {
        emailView.isHidden = true
        lblHeader.text = "Invite/Request"
    }
}
