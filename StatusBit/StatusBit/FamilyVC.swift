//
//  FamilyVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 5/4/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class FamilyVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var selectContactMethod:NSInteger = 0
    var ctb = CustomTabBarVC()
    var arrImages = ["pic15.png","pic10.png","pic12.png","pic13.png","pic14.png","pic11.png","pic9.png","pic8.png","pic7.png","pic2.png"]
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblMyDCN: UILabel!
    @IBOutlet weak var lblFamily: UILabel!
    @IBOutlet weak var collectionViewFamily: UICollectionView!
    
//   Contact Type View
    
    @IBOutlet weak var contactTypeView: UIView!
    
//    MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        ctb.btnStatus.isSelected = true
        self.view.addSubview(ctb.view)
        startDesign()
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width/2
        imgUserProfile.layer.masksToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startDesign() {
        bottomBorder(select: 0, lbl: lblMyDCN)
        bottomBorder(select: 1, lbl: lblFamily)
        contactTypeView.isHidden = true
    }
    
    func bottomBorder(select:NSInteger, lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.gray.cgColor
        }
        else {
            border.borderColor = UIColor.orange.cgColor
        }
        
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  UIScreen.main.bounds.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
// MARK: - Collection View Delegate
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Pcell = collectionView.dequeueReusableCell(withReuseIdentifier: "FamilyCell", for: indexPath) as! FamilyCell
        
        Pcell.imgFamilyMember.layer.cornerRadius = Pcell.imgFamilyMember.frame.width/2
        Pcell.imgFamilyMember.layer.masksToBounds = true
        
        Pcell.imgFamilyMember.image = UIImage(named: arrImages[indexPath.row])
        Pcell.imgShowStatus.image = UIImage(named: "GCoffline.png")
        if indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 9 {
//            Pcell.imgFamilyMember.image = UIImage(named: "GCblue-ppl.png")
            Pcell.imgShowStatus.image = UIImage(named: "GConline.png")
        }
        
        return Pcell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        contactTypeView.isHidden = false
    }

//    MARK: - Action Methods
    
    
    @IBAction func btnGoBackPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSettingPressed(_ sender: Any) {
        let svc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        navigationController?.pushViewController(svc, animated: true)
    }
    @IBAction func btnAddMemberPressed(_ sender: Any) {
        let rvc = storyboard?.instantiateViewController(withIdentifier: "RequestVC") as! RequestVC
        navigationController?.pushViewController(rvc, animated: true)
    }
    
    @IBAction func btnViewProfilePressed(_ sender: Any) {
        selectContactMethod = 1
    }
    
    @IBAction func btnSMSTextPressed(_ sender: Any) {
        selectContactMethod = 2
    }
    
    @IBAction func btnChatpressed(_ sender: Any) {
        selectContactMethod = 3
    }
    
    @IBAction func btnVideoPressed(_ sender: Any) {
        selectContactMethod = 4
    }
    
    @IBAction func btnInboxPressed(_ sender: Any) {
        selectContactMethod = 5
    }
    
    @IBAction func btnNextPressed(_ sender: Any) {
        switch selectContactMethod {
        case 1:
            let epvc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            navigationController?.pushViewController(epvc, animated: true)
            break
            
        case 2:
//            let epvc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//            navigationController?.pushViewController(epvc, animated: true)
            break
            
        case 3:
            let epvc = storyboard?.instantiateViewController(withIdentifier: "SimpleChateVC") as! SimpleChateVC
            navigationController?.pushViewController(epvc, animated: true)
            break
            
        case 4:
            let epvc = storyboard?.instantiateViewController(withIdentifier: "SimpleChateVC") as! SimpleChateVC
            navigationController?.pushViewController(epvc, animated: true)
            break
            
        case 5:
//            let epvc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//            navigationController?.pushViewController(epvc, animated: true)
            break
            
        default: break
            
        }
        
        contactTypeView.isHidden = true
    }
    
    @IBAction func btnHideContactMenuPressed(_ sender: Any) {
        contactTypeView.isHidden = true
    }

}

class FamilyCell: UICollectionViewCell {
    @IBOutlet weak var imgFamilyMember: UIImageView!
    @IBOutlet weak var imgShowStatus: UIImageView!
    
}

