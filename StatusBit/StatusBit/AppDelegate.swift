//
//  AppDelegate.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/3/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit
import Contacts


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate  {

    var window: UIWindow?
//    var contactStore = CNContactStore()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        GIDSignIn.sharedInstance().clientID = "120947692872-d2mvi62q5arq9elop31qvnhb7mmvfhf5.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        return true
    }
    
        func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(
//            application,
//            open: url as URL!,
//            sourceApplication: sourceApplication,
//            annotation: annotation)
        let checkFB = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        let checkGoogle = GIDSignIn.sharedInstance().handle(url as URL!,sourceApplication: sourceApplication,annotation: annotation)
        return checkGoogle || checkFB
        
    }
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
//    }
    
//    @nonobjc public func application(application: UIApplication,
//                     openURL url: URL,
//                     options: [String: AnyObject]) -> Bool {
//        
//        if #available(iOS 9.0, *) {
//            print(UIApplicationOpenURLOptionsKey.sourceApplication._rawValue as String)
//            print(UIApplicationOpenURLOptionsKey.annotation._rawValue as String)
//            
//            return GIDSignIn.sharedInstance().handle(url as URL!,
//                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication._rawValue as String] as! String!,
//                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation._rawValue as String])
//        } else {
//            // Fallback on earlier versions
//            return false
//        }
//    }
    
    func application(application: UIApplication,
                     openURL url: NSURL,
                     sourceApplication: String?,
                     annotation: AnyObject?) -> Bool {
        
        return GIDSignIn.sharedInstance().handle(url as URL!,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error != nil) {
            print(error)
        }
        if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
            print("signed in")
            let navigationController: UINavigationController? = (UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController)
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileView: HomeVC? = (mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC)
            navigationController?.pushViewController(profileView!, animated: false)
            // Forward the user here straight away...
        } else {
            print ("not signed in")
//            let navigationController: UINavigationController? = (UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController)
//            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileView: RegisterVC? = (mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC)
//            navigationController?.pushViewController(profileView!, animated: false)
//             Need to handle the forwarding once they sign in.
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!,dismiss viewController: UIViewController!) {
//        self.dismiss(animated: true, completion: nil)
        print("signed in")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

