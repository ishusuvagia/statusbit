//
//  SimpleChateVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/12/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class SimpleChateVC: UIViewController,LynnBubbleViewDataSource,LynnBubbleViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    @IBOutlet weak var lblTime,lblHeader: UILabel!
    @IBOutlet weak var imgDummy: UIImageView!
    @IBOutlet weak var tblChatData: LynnBubbleTableView!
    var arrChat:Array<LynnBubbleData> = []
    let arrChatData:NSMutableArray = [["Msg":"hi","Type":"Sender","image":"","Time":"01-04-2017"],["Msg":"Hello","Type":"Sender","image":"","Time":"01-04-2017"],
                                      ["Msg":"","Type":"Receiver","image":"http://i.imgur.com/Mi8CAdV.jpg","Time":"01-04-2017"],
                                      ["Msg":"Hi","Type":"Receiver","image":"","Time":"01-04-2017"],
                                      ["Msg":"How r u?","Type":"Sender","image":"","Time":"12-04-2017"],
                                      ["Msg":"Fine.","Type":"Receiver","image":"","Time":"15-04-2017"],
                                      ["Msg":"And U?","Type":"Receiver","image":"","Time":"17-04-2017"],
                                      ["Msg":"","Type":"Receiver","image":"http://i.imgur.com/FkInYhB.jpg","Time":"17-04-2017"],
                                      ["Msg":"","Type":"Sender","image":"http://i.imgur.com/Mi8CAdV.jpg","Time":"18-04-2017"],
                                      ["Msg":"fine","Type":"Sender","image":"http://i.imgur.com/Mi8CAdV.jpg","Time":"18-04-2017"]]
    
    
    @IBOutlet weak var txtFieldMsgType: UITextField!
    @IBOutlet weak var chatView: UIView!
    let picker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblHeader.text = "Swati Kachroo"
        
        getTodayTime()
       
        picker.delegate = self;
        tblChatData.bubbleDelegate = self
        tblChatData.bubbleDataSource = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.testChatData()
    }
    
    func getTodayTime() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        let dateString = formatter.string(from: Date())
        print(dateString)
        
        lblTime.text = "Today \u{2022} \(dateString) "
    }
    
    func selectoption(opt:NSInteger) {
        picker.allowsEditing = false
        if opt == 1 {
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        else {
            picker.sourceType = .camera
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        
    }
    
    //    MARK: - Text Field Delegate
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    
//MARK: - Delegates
    
    private func imagePickerController(_ picker: UIImagePickerController,
                                       didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        //        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //
        //        imgUserProfile.image = chosenImage //4
        
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        let imageURL = info[UIImagePickerControllerReferenceURL] as! NSURL
        //        //        imgUserProfile.contentMode = .scaleAspectFit //3
        imgDummy.image = chosenImage //4
        
        arrChatData.add(["Msg":"","Type":"Sender","image":"\(imageURL)","Time":"03-05-2017"]);
        //        testChatData()
        
        dismiss(animated:true, completion: nil) //5
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    
//    MARK: - Action Method
    
    @IBAction func btnGoBack(_ sender: Any) {
            _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendMsgPressed(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy" //Your New Date format as per requirement change it own
        let newDate = dateFormatter.string(from: Date())
        
        
        arrChatData.add(["Msg":"\(txtFieldMsgType.text!)","Type":"Sender","image":"","Time":"\(newDate)"])
        print(arrChatData)
        testChatData()
        txtFieldMsgType.text = ""
    }
    
    @IBAction func btnMenuPressed(_ sender: Any) {
        
        let controller:SideMenuVc = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuVc") as! SideMenuVc
        controller.view.frame = self.view.bounds;
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
        
    }
    
    @IBAction func btnVideoCallPressed(_ sender: Any) {
        let vvc = storyboard?.instantiateViewController(withIdentifier: "VideoCallVC") as! VideoCallVC
        navigationController?.pushViewController(vvc, animated: true)
    }
    
    @IBAction func btnSharePressed(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image From", message: nil, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            self.selectoption(opt: 0)
        }
        
        let Gallery = UIAlertAction(title: "Gallery", style: .default) { (UIAlertAction) in
            self.selectoption(opt: 1)
        }
        
        let Cancel = UIAlertAction(title: "Cancel", style: .default) { (UIAlertAction) in
            
        }
        
        alert.addAction(camera)
        alert.addAction(Gallery)
        alert.addAction(Cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnCameraPresed(_ sender: Any) {
        self.selectoption(opt: 0)
    }
    
    @IBAction func btnGalleryPressed(_ sender: Any) {
        self.selectoption(opt: 1)
    }
    
//    MARK: - Table View Delegate 

    func testChatData () {
        /*
         let userMe = LynnUserData(userUniqueId: "123", userNickName: "me", userProfileImage: nil, additionalInfo: nil)
         let userSomeone = LynnUserData(userUniqueId: "234", userNickName: "you", userProfileImage: UIImage(named: "ico_girlprofile"), additionalInfo: nil)
         
         let yesterDay = Date().addingTimeInterval(-60*60*24)
         
         
         let bubbleData:LynnBubbleData = LynnBubbleData(userData: userMe, dataOwner: .me, message: "test", messageDate: yesterDay)
         
         self.arrChatTest.append(bubbleData)  //삽입
         
         print(index)
         
         let image_width = LynnAttachedImageData(named: "cat_width.jpg")
         
         self.arrChatTest.append(LynnBubbleData(userData: userSomeone, dataOwner: .someone, message: nil, messageDate: Date(), attachedImage: image_width))  //삽입
         self.arrChatTest.append(bubbleData) //삽입
         
         
         self.tbBubbleDemo.reloadData()
         
         */
        
        arrChat = []
        
        print(arrChatData)
        
        let userMe = LynnUserData(userUniqueId: "123", userNickName: "me", userProfileImage: nil, additionalInfo: nil)
        
        let userSomeone = LynnUserData(userUniqueId: "234", userNickName: "you", userProfileImage: UIImage(named: "ico_girlprofile"), additionalInfo: nil)
        
        //        let yesterDay = Date().addingTimeInterval(-60*60*24)
        
        for index in 0..<arrChatData.count {
            
            let dict = arrChatData[index] as! NSDictionary
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
            
            let date = dateFormatter.date(from: dict["Time"] as! String)
            
            
            if dict["Type"] as! String == "Sender" {
                //                let bubbleData:LynnBubbleData = LynnBubbleDataMine(userID: "123",userNickname: "me" , profile: nil, text: messageMine, image: nil, date: NSDate())
                
                
                
                //                if (dict["Msg"] as! String).isEmpty == true {
                //                    bubbleData = LynnBubbleData(userData: userMe, dataOwner: .me, message: nil, messageDate: date!)
                //                }
                
                if (dict["image"] as! String).isEmpty == false {
                    
                    let urlString:String = (dict["image"] as! String)
                    
                    var imgDataSender = LynnAttachedImageData(url: urlString)
                    
                    
                    if urlString.contains("http://") {
                        imgDataSender = LynnAttachedImageData(url: urlString)
                    }
                    else {
                        imgDataSender = LynnAttachedImageData(image: imgDummy.image!)
                    }

                    self.arrChat.append(LynnBubbleData(userData: userMe, dataOwner: .me, message: nil, messageDate: date!, attachedImage: imgDataSender))
                    
                    if (dict["Msg"] as? String)?.characters.count == 0 {
                        
                        let bubbleData:LynnBubbleData = LynnBubbleData(userData: userMe, dataOwner: .me, message: nil, messageDate: date!)
                        self.arrChat.append(bubbleData)
                    }
                    else {
                        let bubbleData:LynnBubbleData = LynnBubbleData(userData: userMe, dataOwner: .me, message: dict["Msg"] as? String, messageDate: date!)
                        self.arrChat.append(bubbleData)
                    }

                }
//                self.arrChat.append(bubbleData)
            }
                
            else if dict["Type"] as! String == "Receiver" {

                if (dict["image"] as! String).isEmpty == false {
                    
                    let urlString:String = (dict["image"] as! String)
                    let imgDataReceiver = LynnAttachedImageData(url: urlString)
                    self.arrChat.append(LynnBubbleData(userData: userMe, dataOwner: .someone, message: dict["Msg"] as? String, messageDate: date!, attachedImage: imgDataReceiver))
                }
                
                
                if (dict["Msg"] as? String)?.characters.count == 0 {
                    
                    let bubbleData:LynnBubbleData = LynnBubbleData(userData: userSomeone, dataOwner: .someone, message:nil, messageDate: date!)
                    self.arrChat.append(bubbleData)
                }
                else {
                    let bubbleData:LynnBubbleData = LynnBubbleData(userData: userSomeone, dataOwner: .someone, message: dict["Msg"] as? String, messageDate: date!)
                    self.arrChat.append(bubbleData)
                }
                
//                self.arrChat.append(bubbleData)
            }
        }
        
        self.tblChatData.reloadData()
    }
    
    
    func bubbleTableView(numberOfRows bubbleTableView:LynnBubbleTableView) -> Int {
        return arrChat.count
    }
    
    func bubbleTableView(dataAt index:Int, bubbleTableView:LynnBubbleTableView) -> LynnBubbleData {
        return arrChat[index]
    }

}

