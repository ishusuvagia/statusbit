//
//  RegisterVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/3/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import Foundation
import UIKit


class RegisterVC: UIViewController,UITextFieldDelegate,FBSDKLoginButtonDelegate, GIDSignInUIDelegate {
    
    /**
     Sent to the delegate when the button was used to login.
     - Parameter loginButton: the sender
     - Parameter result: The results of the login
     - Parameter error: The error (if any) from the login
     */
    
    @IBOutlet weak var btnFBLogin: FBSDKLoginButton!
    
    @IBOutlet weak var btnGoogleSignin: GIDSignInButton!
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("User Logged In")
        
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
                let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                navigationController?.pushViewController(hvc, animated: true)
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
            print("signed in")
            let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            navigationController?.pushViewController(hvc, animated: true)
            
            // Forward the user here straight away...
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }

    
//    MARK: - Register View
    
    var logout: NSInteger = 0
    
    
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var btnTAndC: UIButton!
    @IBOutlet weak var txtFieldFirstName: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldLastName: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldRegEmail: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldRegPhoneNu: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldRegPsd: VMFloatLabelTextField!
    
//    MARK: - Login View
        
    @IBOutlet weak var btnLoginTC: UIButton!
    

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var txtFieldEmail: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldPhoneNumber: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldPassword: VMFloatLabelTextField!
    
    
//    MARK: - IBOutlet
    
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var lblWelComeStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblWelComeStatus.adjustsFontSizeToFitWidth = true
        loginView.isHidden = true
        registerView.isHidden = true
        
//        GIDSignIn.sharedInstance().clientID = "120947692872-d2mvi62q5arq9elop31qvnhb7mmvfhf5.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().uiDelegate = self;
        GIDSignIn.sharedInstance().signInSilently()
        
        
        
        btnFBLogin.readPermissions = ["public_profile", "email", "user_friends"]
        btnFBLogin.delegate = self
        
        btn2.isSelected = true
    
        if logout == 1 {
            GIDSignIn.sharedInstance().signOut()
            FBSDKLoginManager().logOut()
            logout = 0
        }
        
        
        
        drawLine()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func bottomBorder(select:NSInteger, txtField:UITextField) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.orange.cgColor
        }
        else {
            border.borderColor = UIColor.lightGray.cgColor
        }
        
        border.frame = CGRect(x: 0, y: txtField.frame.size.height - width, width:  txtField.frame.size.width, height: txtField.frame.size.height)
        
        border.borderWidth = width
        txtField.layer.addSublayer(border)
        txtField.layer.masksToBounds = true
    }
    
    func unSelecteAllBtn() {
        btn1.isSelected = false
        btn2.isSelected = false
        btn3.isSelected = false
    }
    
    func drawLine() {
        bottomBorder(select: 0, txtField: txtFieldEmail)
        bottomBorder(select: 0, txtField: txtFieldPassword)
        bottomBorder(select: 0, txtField: txtFieldPhoneNumber)
        bottomBorder(select: 0, txtField: txtFieldRegEmail)
        bottomBorder(select: 0, txtField: txtFieldRegPsd)
        bottomBorder(select: 0, txtField: txtFieldRegPhoneNu)
        bottomBorder(select: 0, txtField: txtFieldLastName)
        bottomBorder(select: 0, txtField: txtFieldFirstName)
        
    }
    
    func showAlert(msg:String) {
        let  alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        
        let okAcction = UIAlertAction(title: "OK", style: .cancel) { (UIAlertAction) in
            
        }
        
        alert.addAction(okAcction)
        self.present(alert, animated: true, completion: nil)
        
    }

    
    //    MARK: - Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.becomeFirstResponder()
        if textField == txtFieldRegPhoneNu || textField == txtFieldPhoneNumber {
            IQKeyboardManager.sharedManager().enableAutoToolbar = true
        }
        else {
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
        }
        bottomBorder(select: 1, txtField: textField)
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
//    MARK: - Google SignIn Method
    
    @IBAction func signupWithGoogle(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signIn()
    }
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//        myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
//    MARK: - Action Method
    
    @IBAction func btnShowTCPressed(_ sender: Any) {
        let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
        gcvc.page = 1
        navigationController?.pushViewController(gcvc, animated: true)
    }
    @IBAction func btn1Pressed(_ sender: Any) {
        if btn1.isSelected == true {
            btn1.isSelected = false
        }
        else {
            unSelecteAllBtn()
            btn1.isSelected = true
        }
    }
    
    @IBAction func btn2Pressed(_ sender: Any) {
        if btn2.isSelected == true {
            btn2.isSelected = false
        }
        else {
            unSelecteAllBtn()
            btn2.isSelected = true
        }
    }
    
    @IBAction func btn3Pressed(_ sender: Any) {
        if btn3.isSelected == true {
            btn3.isSelected = false
        }
        else {
            unSelecteAllBtn()
            btn3.isSelected = true
        }
    }
    
    @IBAction func btnHealthCareProviderPressed(_ sender: Any) {
        let hcpvc = storyboard?.instantiateViewController(withIdentifier: "HealthCareProviderVC") as! HealthCareProviderVC
        navigationController?.pushViewController(hcpvc, animated: true)
    }
    
    @IBAction func loginWithEmail(_ sender: Any) {
        loginView.isHidden = true
        registerView.isHidden = false
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        loginView.isHidden = false
        registerView.isHidden = true
    }
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let letters = NSCharacterSet.decimalDigits.inverted
        
        if txtFieldEmail.text?.characters.count == 0 || txtFieldPassword.text?.characters.count == 0 || txtFieldPhoneNumber.text?.characters.count == 0 {
            showAlert(msg: "Please enter all fields.")
        }
        else if !(emailTest.evaluate(with: txtFieldEmail.text)) {
            showAlert(msg: "Please enter valid email.")
        }
            
        else if txtFieldPhoneNumber.text?.rangeOfCharacter(from: letters) != nil {
            showAlert(msg: "Please enter valid phone number.")
        }
        else {
            let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            navigationController?.pushViewController(hvc, animated: true)
        }
    }
    
    @IBAction func btnRegisterPressed(_ sender: Any) {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let letters = NSCharacterSet.decimalDigits.inverted

        
        if txtFieldRegEmail.text?.characters.count == 0 || txtFieldRegPsd.text?.characters.count == 0 || txtFieldRegPhoneNu.text?.characters.count == 0 || txtFieldFirstName.text?.characters.count == 0 || txtFieldLastName.text?.characters.count == 0 {
            showAlert(msg: "Please enter all fields.")
        }
            
        else if !(emailTest.evaluate(with: txtFieldRegEmail.text)) {
            showAlert(msg: "Please enter valid email.")
        }
        
        else if txtFieldRegPhoneNu.text?.rangeOfCharacter(from: letters) != nil {
            showAlert(msg: "Please enter valid phone number.")
        }
            
        else {
            let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            navigationController?.pushViewController(hvc, animated: true)
        }
    }
    
    @IBAction func btnTermsCondition(_ sender: Any) {
        if btnTAndC.isSelected == true {
            btnTAndC.isSelected = false
        }
        else {
            btnTAndC.isSelected = true
        }
    }
    @IBAction func btnLoginTCPressed(_ sender: Any) {
        if btnLoginTC.isSelected == true {
            btnLoginTC.isSelected = false
        }
        else {
            btnLoginTC.isSelected = true
        }
    }
    
}
