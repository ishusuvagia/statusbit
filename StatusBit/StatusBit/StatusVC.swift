//
//  StatusVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 5/4/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class StatusVC: UIViewController {
    
    @IBOutlet weak var txtViewStatus: UITextView!
//    MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK: - Text View Methods  
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //        if (textView.text == "What's on your \nmind ?") {
        textView.text = ""
        textView.textColor = UIColor.black
        //optional
        //        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == "") {
            textView.text = "What's on your \nmind ?"
            textView.textColor = UIColor.lightGray
            //optional
        }
        textView.resignFirstResponder()
    }

    
//    MARK: - Action Methods
    
    @IBAction func btnAddStatusPressed(_ sender: Any) {
        txtViewStatus.resignFirstResponder()
    }
    
    @IBAction func btnGoBackPressed(_ sender: Any) {
        txtViewStatus.resignFirstResponder()
        _ = navigationController?.popViewController(animated: true)
    }

}
