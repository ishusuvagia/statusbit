//
//  SideMenuVc.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/7/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit
import ContactsUI

class SideMenuVc: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,CNContactPickerDelegate {

    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserStatus: UILabel!
    @IBOutlet weak var lblNavigation: UILabel!
    
    @IBOutlet weak var sideMenuView: UIView!
    var arrMenuName:NSMutableArray = ["Home","My Profile","Invite via Facebook","Share","Help","About Status Bits","Logout"]
    
    var arrMenuImage:NSMutableArray = ["Sidehome.png","Sideprofile.png","Sidefb-invite.png","Sideshare.png","Sidehelp.png","Sideabout.png","Sidelogout.png"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomBorder(lbl: lblNavigation)
        
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        imgProfile.layer.masksToBounds = true
        
        let tape = UITapGestureRecognizer(target: self, action:#selector(hideSideMenu))
        tape.delegate = self
        self.view.addGestureRecognizer(tape)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: sideMenuView))! {
            return false
        }
        return true
    }
    
    func hideSideMenu() {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
     }
    
    func bottomBorder(lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  lbl.frame.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
    @IBAction func btnEditPressed(_ sender: Any) {
        let gcvc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        navigationController?.pushViewController(gcvc, animated: true)
        
        hideSideMenu()
    }
    
    
    
//    func showContact() {
//        if #available(iOS 9.0, *) {
//            let status = CNContactStore.authorizationStatus(for: .contacts)
//            if status == .denied || status == .restricted {
//                presentSettingsActionSheet()
//                return
//            }
//            
//        } else {
//            // Fallback on earlier versions
//        }
//        
//        
//        // open it
//        
//        if #available(iOS 9.0, *) {
//            let store = CNContactStore()
//            store.requestAccess(for: .contacts) { granted, error in
//                guard granted else {
//                    DispatchQueue.main.async {
//                        self.presentSettingsActionSheet()
//                    }
//                    return
//                }
//            }
//        }
//        else {
//            // Fallback on earlier versions
//        }
//        
//            
//            // get the contacts
//            
//        if #available(iOS 9.0, *) {
//            var contacts = [CNContact]()
//        } else {
//            // Fallback on earlier versions
//        }
//            let request = CNContactFetchRequest(keysToFetch: [CNContactIdentifierKey as NSString, CNContactFormatter.descriptorForRequiredKeys(for: .fullName)])
//            do {
//                try store.enumerateContacts(with: request) { contact, stop in
//                    contacts.append(contact)
//                }
//            } catch {
//                print(error)
//            }
//            
//            // do something with the contacts array (e.g. print the names)
//            
//            if #available(iOS 9.0, *) {
//                let formatter = CNContactFormatter()
//                formatter.style = .fullName
//                for contact in contacts {
//                    print(formatter.string(from: contact) ?? "???")
//                }
//
//            } else {
//                // Fallback on earlier versions
//            }
//        }
//    }
//    
//    func presentSettingsActionSheet() {
//        let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to contacts in order to ...", preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
//            let url = URL(string: UIApplicationOpenSettingsURLString)!
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url)
//            } else {
//                // Fallback on earlier versions
//            }
//        })
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
////        present(alert, animated: true)
//    }
    
//    MARK: - CNContact Delegate
    
    @available(iOS 9.0, *)
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        contacts.forEach { contact in
            for number in contact.phoneNumbers {
                let phoneNumber = number.value
                print("number is = \(phoneNumber)")
            }
        }
    }
    
    @available(iOS 9.0, *)
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
    
    
//    MARK: - Table view Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuName.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        cell.imgMenuIcon.image = UIImage(named: arrMenuImage[indexPath.row] as!  String)
        cell.lblMenuName.text = arrMenuName[indexPath.row] as?  String
        
        return cell;
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row == 0 {
//            "Home"
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            navigationController?.pushViewController(gcvc, animated: true)
            
        }
            
        else if indexPath.row == 1 {
//            "My Profile"
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 2 {
//            "Invite friends via FB"
            
//            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
//            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 3 {
//            "Share"
            
            if #available(iOS 9.0, *) {
                let cnPicker = CNContactPickerViewController()
                cnPicker.delegate = self
                self.present(cnPicker, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
//            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
//            navigationController?.pushViewController(gcvc, animated: true)
            
        }
            
        else if indexPath.row == 4 {
//            "Help"
            
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
            gcvc.page = 3
            navigationController?.pushViewController(gcvc, animated: true)
            
            
//            let gcvc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 5 {
//            "About Status Bits"
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
            gcvc.page = 2
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 6 {
//            "Logout"
            
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            gcvc.logout = 1
            navigationController?.pushViewController(gcvc, animated: true)
            
//            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
//            gcvc.page = 2
//            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 7 {
//            "Logout"
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            gcvc.logout = 1
            navigationController?.pushViewController(gcvc, animated: true)
        }
        
        hideSideMenu()
    
    }
    
}


class MenuCell: UITableViewCell {
    
    @IBOutlet weak var imgMenuIcon: UIImageView!
    @IBOutlet weak var lblMenuName: UILabel!
}
