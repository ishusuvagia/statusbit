//
//  LynnBubbleViewHeaderCell.swift
//  To my love Derrick and LynnA
//
//  Created by Lou Hwang on 2015. 10. 30..
//  Copyright © 2015 Lou Hwang. All rights reserved.
//

import UIKit

class LynnBubbleViewHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var HeaderBGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        lbDate.layer.cornerRadius = lbDate.frame.height/2
        lbDate.layer.masksToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDate(date:Date, withDay:Bool) {
        
        
        let today : NSDate = Date() as NSDate
        
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: date)
        let date2 = calendar.startOfDay(for: today as Date)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        let monthComponents = calendar.dateComponents([.month], from: date1, to: date2)
        
        if components.day == 0 {
            self.lbDate.text = "Today"
        }
        else if components.day == 1 {
            self.lbDate.text = "YesterDay"
        }
        else {
            self.lbDate.text = "\((components.day)!) days ago"
            if monthComponents.month! > 0 {
                self.lbDate.text = "\((monthComponents.month)!) months ago"
            }
        }
    }
}
