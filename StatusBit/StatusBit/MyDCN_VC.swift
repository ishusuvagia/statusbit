//
//  MyDCN_VC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/4/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class MyDCN_VC: UIViewController {

    var ctb = CustomTabBarVC()
    @IBOutlet weak var imgUserMainProfile: UIImageView!
 
    override func viewDidLoad() {
        super.viewDidLoad()

        
        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        ctb.btnMyDCN.isSelected = true
        self.view.addSubview(ctb.view)
        
        
        imgUserMainProfile.layer.cornerRadius = imgUserMainProfile.frame.width/2
        imgUserMainProfile.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSettingPressed(_ sender: Any) {
        let svc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        navigationController?.pushViewController(svc, animated: true)
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        let controller:SideMenuVc = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuVc") as! SideMenuVc
        controller.view.frame = self.view.bounds;
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }
    
    @IBAction func btnFriend(_ sender: Any) {
        
        
    }

    @IBAction func btnHealthCareProfPressed(_ sender: Any) {
        
    }
    
    @IBAction func btnViewAllPressed(_ sender: Any) {
        let mdcn = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
        navigationController?.pushViewController(mdcn, animated: true)
    }

    @IBAction func btnFamilyPressed(_ sender: Any) {
        let mdcn = storyboard?.instantiateViewController(withIdentifier: "FamilyVC") as! FamilyVC
        navigationController?.pushViewController(mdcn, animated: true)
    }
    
    @IBAction func btnCommunitySupporter(_ sender: Any) {
        
    }
    
    @IBAction func btnCareForOtherPressed(_ sender: Any) {
        
        let mdcn = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        navigationController?.pushViewController(mdcn, animated: true)
        
    }
    
    @IBAction func btnInvitePressed(_ sender: Any) {
        let rvc = storyboard?.instantiateViewController(withIdentifier: "RequestVC") as! RequestVC
        navigationController?.pushViewController(rvc, animated: true)                    
    }
    
}
