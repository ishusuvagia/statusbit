//
//  EditProfileVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/10/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    var ctb = CustomTabBarVC()
    let picker = UIImagePickerController()
    @IBOutlet weak var imgUserProfile: UIImageView!
    
    // MARK: - Timeline View
    
    var arrDayName:NSMutableArray = ["Today","Yesterday","Satureday"]
    var arrDate:NSMutableArray = ["4:30 AM","3:45 PM","11:18 PM"]
    var arruserTimeline:NSMutableArray = ["You have been much more to me than just a doctor. You have been my therapist, supporter, friend, well wisher and angel in disguise.Thank you.","To study the phenomena of disease without books is to sail an uncharted sea, while to study books without patients is not to go to sea at all.The physician should not treat the disease but the patient who is suffering from it","Flowers always make people better, happier, and more helpful; they are sunshine, food and medicine to the mind.A merry heart doeth good like a medicine: but a broken spirit drieth the bones."]
    var arrImages = ["pic2.png","pic9.png","pic14.png","pic12.png","pic13.png","pic10.png","pic11.png","pic8.png","pic7.png","pic15.png"]
    
    @IBOutlet weak var btnTimeline: UIButton!
    @IBOutlet weak var TimelineView: UIView!
    @IBOutlet weak var showDatePicker: UIView!
    
    
    
    // MARK: - Profile Info View
    
    @IBOutlet weak var btnProfileInfo: UIButton!
    @IBOutlet weak var btnAddProfile: UIButton!
   
    @IBOutlet weak var ProfileInfoview: UIView!
    @IBOutlet weak var lblProfileBirthDate,lblUserProfileUsrName,lblProfilAddress,lblProfileName,lblUserProfileEmail,lblEmail: UILabel!
    @IBOutlet weak var lblChoiceBD: UILabel!
    
    @IBOutlet weak var txtFieldAddress,txtFieldUserName,txtFieldUserProfileName,txtFieldEmail: UITextField!
    @IBOutlet weak var choiceBirthDate: UIButton!
    
    // MARK: - Photo View

    @IBOutlet weak var btnPhotos: UIButton!
    @IBOutlet weak var RecentPhoto: UIView!
    @IBOutlet weak var collectionOfImage: UICollectionView!
    
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        self.view.addSubview(ctb.view)
        
        disableTxtField(enable: 0)
        picker.delegate = self
        btnProfileInfo.isSelected = true
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width/2
        imgUserProfile.layer.masksToBounds = true
        
        let screenWidth = UIScreen.main.bounds.width
        
        let tapped = UITapGestureRecognizer(target: self, action: #selector(hideDatePicker))
        tapped.delegate = self
        self.view.addGestureRecognizer(tapped)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: ((screenWidth - 17)/3), height: screenWidth/(3*1.7))
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionOfImage!.collectionViewLayout = layout
        
        startUpDesign()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        
//        if (touch.view?.isDescendant(of: addDCNView))! {
//            return true
//        }
//        return false
//    }
    
    func startUpDesign() {
        bottomBorder(lbl: lblEmail)
        bottomBorder(lbl: lblProfileName)
        bottomBorder(lbl: lblProfilAddress)
        bottomBorder(lbl: lblProfileBirthDate)
        bottomBorder(lbl: lblUserProfileUsrName)
        
        showDatePicker.isHidden = true
    }
    
    func hideDatePicker() {
        showDatePicker.isHidden = true
        
        disableTxtField(enable: 1)
    }
    
    func disableTxtField(enable:NSInteger) {
       
        if enable == 1 {
            txtFieldAddress.isUserInteractionEnabled = true
            txtFieldUserName.isUserInteractionEnabled = true
            txtFieldUserProfileName.isUserInteractionEnabled = true
            txtFieldEmail.isUserInteractionEnabled = true
            
            txtFieldAddress.resignFirstResponder()
            txtFieldUserName.resignFirstResponder()
            txtFieldUserProfileName.resignFirstResponder()
            txtFieldEmail.resignFirstResponder()
            
            btnAddProfile.isHidden = false
        }
        else {
            txtFieldAddress.isUserInteractionEnabled = false
            txtFieldUserName.isUserInteractionEnabled = false
            txtFieldUserProfileName.isUserInteractionEnabled = false
            txtFieldEmail.isUserInteractionEnabled = false
            
            btnAddProfile.isHidden = true
        }
    }
    
    func selectoption(opt:NSInteger) {
        picker.allowsEditing = false
        if opt == 1 {
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        else {
            picker.sourceType = .camera
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        
    }
    
    func bottomBorder(lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  lbl.frame.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
//    MARK: - Text Field Delegate
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: - Delegates
    private func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        imgUserProfile.contentMode = .scaleAspectFit //3
        imgUserProfile.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
//        imgUserProfile.contentMode = .scaleAspectFit //3
        imgUserProfile.image = chosenImage //4
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width/2
        imgUserProfile.layer.masksToBounds = true
        dismiss(animated:true, completion: nil) //5
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func dueDateChanged(sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
//        dateFormatter.timeStyle = DateFormatter.Style.none
        lblProfileBirthDate.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func choiceBirthDatePressed(_ sender: Any) {
        
        showDatePicker.isHidden = false
    }
//
    
    
    @IBAction func btnProfileInfoPressed(_ sender: Any) {
        
        if btnProfileInfo.isSelected == true {
            btnProfileInfo.isSelected = false
        }
        else {
            
            btnProfileInfo.isSelected = true
            btnTimeline.isSelected = false
            btnPhotos.isSelected = false
            
            ProfileInfoview.isHidden = false
            TimelineView.isHidden = true
            RecentPhoto.isHidden = true
        }
    }
    
    @IBAction func btnTimelinePressed(_ sender: Any) {
        
        if btnTimeline.isSelected == true {
            btnTimeline.isSelected = false
        }
        else {
            
            btnTimeline.isSelected = true
            btnProfileInfo.isSelected = false
            btnPhotos.isSelected = false
            
            ProfileInfoview.isHidden = true
            TimelineView.isHidden = false
            RecentPhoto.isHidden = true
        }
    }
    
    @IBAction func btnPhotosPressed(_ sender: Any) {
        if btnPhotos.isSelected == true {
            btnPhotos.isSelected = false
        }
        else {
            btnProfileInfo.isSelected = false
            btnTimeline.isSelected = false
            btnPhotos.isSelected = true
            
            ProfileInfoview.isHidden = true
            TimelineView.isHidden = true
            RecentPhoto.isHidden = false
            
        }
    }
    
    @IBAction func btnEditPressed(_ sender: Any) {
        disableTxtField(enable: 1)
        txtFieldEmail.becomeFirstResponder()
    }
    
    @IBAction func btnAddProfilePressed(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image From", message: nil, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            self.selectoption(opt: 0)
        }
        
        let Gallery = UIAlertAction(title: "Gallery", style: .default) { (UIAlertAction) in
            self.selectoption(opt: 1)
        }
        
        let Cancel = UIAlertAction(title: "Cancel", style: .default) { (UIAlertAction) in
            
        }
        
        alert.addAction(camera)
        alert.addAction(Gallery)
        alert.addAction(Cancel)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnShowDatePressed(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //        dateFormatter.timeStyle = DateFormatter.Style.none
        lblChoiceBD.text = dateFormatter.string(from: (sender as AnyObject).date)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //    MARK: - Table view Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDate.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineCell") as! TimelineCell
        
        cell.lblDayName.text = arrDayName[indexPath.row] as?  String
        cell.lblTimelineDate.text = arrDate[indexPath.row] as?  String
        cell.lblUserTimeline.text = arruserTimeline[indexPath.row] as?  String
        cell.lblUserTimeline.adjustsFontSizeToFitWidth = true
        
        return cell;
    }
    
    // MARK: - Collection View Delegate
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Pcell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentImageCell", for: indexPath) as! RecentImageCell
        
        Pcell.imgRecent.image = UIImage(named: arrImages[indexPath.row])
        
        return Pcell
    }
    
//    var collectionViewContentSize: CGSize {
//        let width = (UIScreen.main.bounds.width - 10)/3
//        return CGSize(width: width, height: width*1.44)
//    }
    
//    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//        let width = (collectionOfImage.frame.width-15)/3
//        return CGSize(width: width, height: width);
//    }
}

class TimelineCell : UITableViewCell {
    
    @IBOutlet weak var lblDayName: UILabel!
    @IBOutlet weak var lblTimelineDate: UILabel!
    @IBOutlet weak var imgLine: UIImageView!
    @IBOutlet weak var lblUserTimeline: UILabel!
    
}

class  RecentImageCell  : UICollectionViewCell {
    
    @IBOutlet weak var imgRecent: UIImageView!
}
