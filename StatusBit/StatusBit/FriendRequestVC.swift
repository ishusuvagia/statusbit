//
//  FriendRequestVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 5/4/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class FriendRequestVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblFrndReq: UITableView!
    var mutableStrPP = NSMutableAttributedString()
    var arrImages = ["pic15.png","pic10.png","pic12.png","pic13.png","pic14.png","pic11.png","pic9.png","pic8.png","pic7.png","pic2.png"]
    
//    MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func attributeStr() -> NSMutableAttributedString {
        
        let str1Attribute = [NSForegroundColorAttributeName:UIColor.black, NSFontAttributeName:UIFont(name: "AppleSDGothicNeo-Bold", size: 15.0)]
        
        let str2Attribute = [NSForegroundColorAttributeName:UIColor.black, NSFontAttributeName:UIFont(name: "AppleSDGothicNeo-Thin", size: 15.0)]
        
        mutableStrPP = NSMutableAttributedString(string: "StatusBit StatusBit (StatusBit)", attributes: str1Attribute)
        
        let partTwo = NSMutableAttributedString(string: "Sent you a friend request.", attributes: str2Attribute)
        
        mutableStrPP.append(partTwo)
        
//        lblPP.attributedText = mutableStrPP
        return mutableStrPP
    }
    
    func btnAcceptPressed(sender:UIButton) {
        print("Accept Pressed \(sender.tag)")
        
        let indexpath = IndexPath(row: sender.tag, section: 0)
        print("Index : \(indexpath)")
        let frndCell : FrndReqCell = tblFrndReq.cellForRow(at: indexpath) as! FrndReqCell
        
        frndCell.backgroundColor = UIColor(red:0.92, green:0.95, blue:0.99, alpha:1.0)
        
        frndCell.btnAccept.setTitle("Accepted", for: .normal)
        frndCell.btnAccept.backgroundColor = UIColor(red:0.21, green:0.72, blue:0.05, alpha:1.0)
        frndCell.btnDecline.setTitle("Clear", for: .normal)
        
        
    }
    
    func btnDeclinePressed(sender:UIButton) {
        print("Decline Pressed \(sender.tag)")
    }
    
//    MARK: - Table View Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FrndReqCell") as! FrndReqCell
        cell.selectionStyle = .none
        
        cell.imgFrnd.layer.cornerRadius = cell.imgFrnd.frame.width/2
        cell.imgFrnd.layer.masksToBounds = true
        
        cell.imgFrnd.image = UIImage(named: arrImages[indexPath.row])
        cell.lblShowName.attributedText = attributeStr()
        
        cell.btnAccept.addTarget(self, action: #selector(btnAcceptPressed), for: .touchUpInside)
        cell.btnAccept.tag = indexPath.row
        cell.btnDecline.tag = indexPath.row
        cell.btnDecline.addTarget(self, action: #selector(btnDeclinePressed), for: .touchUpInside)
        
        
        
        return cell
    }
    
//    MARK: - Action Methods

    @IBAction func btnGoBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}

class FrndReqCell: UITableViewCell {
    
    @IBOutlet weak var imgFrnd: UIImageView!
    @IBOutlet weak var lblShowName: UILabel!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    
}
