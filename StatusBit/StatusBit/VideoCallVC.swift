//
//  VideoCallVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 5/5/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit
import AVFoundation

class VideoCallVC: UIViewController {
    
    
    @IBOutlet weak var userCameraView: UIView!
    var session: AVCaptureSession?
    var input: AVCaptureDeviceInput?
    var output: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var camera:AVCaptureDevice?
    
    
//    MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        callSetUp(option: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callSetUp(option:NSInteger) {
        
        //Initialize session an output variables this is necessary
        
        session = AVCaptureSession()
        output = AVCaptureStillImageOutput()
        
//        var camera:AVCaptureDevice //= getDevice(position: .back)
        
        if option == 1 {
            camera = getDevice(position: .front)!
        }
        else {
            camera = getDevice(position: .back)!
        }
        
        
        do {
            
            input = try AVCaptureDeviceInput(device: camera)
            
        }
        catch let error as NSError {
            
            print(error)
            input = nil
            
        }
        
        if(session?.canAddInput(input) == true){
            
            session?.addInput(input)
            output?.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
            
            if(session?.canAddOutput(output) == true) {
                
                session?.addOutput(output)
                previewLayer = AVCaptureVideoPreviewLayer(session: session)
                previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
                previewLayer?.frame = userCameraView.bounds
                userCameraView.layer.addSublayer(previewLayer!)
                session?.startRunning()
            }
        }
    }
    
    //Get the device (Front or Back)
    
    func getDevice(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        
        let devices: NSArray = AVCaptureDevice.devices() as NSArray;
        
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            
            if(deviceConverted.position == position){
                return deviceConverted
            }
        }
        return nil
    }

    
    
//    MARK: - Action Methods

    @IBAction func btnCallEndPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangeCameraPressed(_ sender: Any) {
        

            if camera?.position == AVCaptureDevicePosition.front {
                callSetUp(option: 0)
            }
            else {
                callSetUp(option: 1)
            }
        
        
    }
    
    @IBAction func btnVoiceCallPressed(_ sender: Any) {
        
    }
    
    @IBAction func btnVideoCallPressed(_ sender: Any) {
        
    }
    
    
}
