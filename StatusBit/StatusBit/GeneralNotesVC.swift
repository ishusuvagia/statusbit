//
//  GeneralNotesVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/11/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class GeneralNotesVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var lblHeader: UILabel!
    
    var showDetailPage :NSInteger = 0
    
    @IBOutlet weak var btnEditNotes: UIButton!
    var ctb = CustomTabBarVC()
    var arrMainData:NSMutableArray = [["Date":"29","Day":"THU","Title":"StatusBit","Details":"You have been much more to me than just a doctor. You have been my therapist, supporter, friend, well wisher and angel in disguise.Thank you."],
                                     ["Date":"27","Day":"SUN","Title":"Meet with friends","Details":"To study the phenomena of disease without books is to sail an uncharted sea, while to study books without patients is not to go to sea at all"],
                                     ["Date":"23","Day":"MON","Title":"Enjoyment at child care hostel","Details":"A merry heart doeth good like a medicine: but a broken spirit drieth the bones."]]
    var arrImages = ["pic2.png","pic9.png","pic14.png","pic12.png","pic13.png","pic10.png","pic11.png","pic8.png","pic7.png","pic15.png"]
    
    //    MARK: - DETAIL VIEW
    
    @IBOutlet weak var btnImageChange: UIButton!
    @IBOutlet weak var DetailView: UIView!
    @IBOutlet weak var imgDetailView: UIImageView!
    @IBOutlet weak var lblTitleDetail: UILabel!
    @IBOutlet weak var lblDetailStatus: UILabel!
    @IBOutlet weak var lblPostTime: UILabel!
    @IBOutlet weak var textViewForDetail: UITextView!
    @IBAction func btnChageImage(_ sender: Any) {
    }
    
    //    MARK: - METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        self.view.addSubview(ctb.view)
        lblHeader.text =  "Health Summary"
        DetailView.isHidden = true
        
        btnEditNotes.isHidden = true
        
        disableTxtField(enable: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func disableTxtField(enable:NSInteger) {
        
        if enable == 1 {
            textViewForDetail.isUserInteractionEnabled = true
            btnImageChange.isHidden = false
        }
        else {
            textViewForDetail.isUserInteractionEnabled = false
            btnImageChange.isHidden = true
        }
    }
    
    @IBAction func btnEditPressed(_ sender: Any) {
        disableTxtField(enable: 1)
    }
    
    @IBAction func btnAddNewNotesPressed(_ sender: Any) {
    }
    
    @IBAction func btnBack(_ sender: Any) {
        if showDetailPage == 1 {
            
            btnEditNotes.isHidden = true
            lblHeader.text =  "Health Summary"
            DetailView.isHidden = true
            showDetailPage = 0
            
        }
        else {
            _ = navigationController?.popViewController(animated: true)
        }
    }

    //    MARK: - Table view Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as! NotesCell
        
        cell.selectionStyle = .none
        
//        if indexPath.row < arrMainData.count {
//            cell.backgroundColor = UIColor.white
//        }
        
        if (indexPath.row%2) == 0 {
            cell.lblSideLine.backgroundColor = UIColor(red:0.93, green:0.48, blue:0.05, alpha:1.0)
            cell.imgDetailDisclouser.image = UIImage(named: "orange-arrow.png")
        }
        else {
            cell.lblSideLine.backgroundColor = UIColor(red:0.14, green:0.28, blue:0.65, alpha:1.0)
            cell.imgDetailDisclouser.image = UIImage(named: "blue-arrow.png")
        }
        
        let dictShow : NSDictionary = arrMainData[indexPath.row] as! NSDictionary
        
        cell.lblDate.text = dictShow["Date"] as? String
        cell.lblDay.text = dictShow["Day"] as? String
        cell.lblTitle.text = dictShow["Title"] as? String
        cell.lblDetail.text = dictShow["Details"] as? String
        cell.backgroundColor = UIColor.white
        
        return cell;
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        showDetailPage = 1
        
        lblHeader.text =  "Notes Detail"
        btnEditNotes.isHidden = false
        
        let dictShowDetail : NSDictionary = arrMainData[indexPath.row] as! NSDictionary
        
        lblTitleDetail.text = dictShowDetail["Title"] as? String
        lblDetailStatus.text = dictShowDetail["Details"] as? String
        imgDetailView.image = UIImage(named: arrImages[indexPath.row])
        
        
//        textViewForDetail.text = dictShowDetail["Details"] as? String
        
        DetailView.isHidden = false
        
    }

}

class NotesCell : UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgDetailDisclouser: UIImageView!
    @IBOutlet weak var lblSideLine: UILabel!
   
}
