//
//  SideMenuVc.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/7/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class SideMenuVc: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserStatus: UILabel!
    @IBOutlet weak var lblNavigation: UILabel!
    
    @IBOutlet weak var sideMenuView: UIView!
    var arrMenuName:NSMutableArray = ["Home","Notes General","Calendar","Group Chat","Setting","Help","About Status Bits","Logout"]
    
    var arrMenuImage:NSMutableArray = ["Sidehome.png","Sidenotes.png","Sidecalender.png","Sidechat.png","Sidesetting.png","Sidehelp.png","Sideabout.png","Sidelogout.png"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomBorder(lbl: lblNavigation)
        
        let tape = UITapGestureRecognizer(target: self, action:#selector(hideSideMenu))
        tape.delegate = self
        self.view.addGestureRecognizer(tape)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: sideMenuView))! {
            return false
        }
        return true
    }
    
    func hideSideMenu() {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
     }
    
    func bottomBorder(lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  lbl.frame.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
//    MARK: - Table view Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuName.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        cell.imgMenuIcon.image = UIImage(named: arrMenuImage[indexPath.row] as!  String)
        cell.lblMenuName.text = arrMenuName[indexPath.row] as?  String
        
        return cell;
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            navigationController?.pushViewController(gcvc, animated: true)
            
        }
            
        else if indexPath.row == 1 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GeneralNotesVC") as! GeneralNotesVC
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 2 {
//            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
//            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 3 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
            navigationController?.pushViewController(gcvc, animated: true)
            
        }
            
        else if indexPath.row == 4 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 5 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
            gcvc.page = 3
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 6 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
            gcvc.page = 2
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 7 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            gcvc.logout = 1
            navigationController?.pushViewController(gcvc, animated: true)
        }
        
        hideSideMenu()
    
    }
    
}


class MenuCell: UITableViewCell {
    
    @IBOutlet weak var imgMenuIcon: UIImageView!
    @IBOutlet weak var lblMenuName: UILabel!
}
