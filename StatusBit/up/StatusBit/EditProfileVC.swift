//
//  EditProfileVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/10/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var ctb = CustomTabBarVC()
    
    // MARK: - Timeline View
    
    var arrDayName:NSMutableArray = ["Today","Yesterday","Satureday"]
    var arrDate:NSMutableArray = ["4:30 AM","3:45 PM","11:18 PM"]
    var arruserTimeline:NSMutableArray = ["You have been much more to me than just a doctor. You have been my therapist, supporter, friend, well wisher and angel in disguise.Thank you.","To study the phenomena of disease without books is to sail an uncharted sea, while to study books without patients is not to go to sea at all.The physician should not treat the disease but the patient who is suffering from it","Flowers always make people better, happier, and more helpful; they are sunshine, food and medicine to the mind.A merry heart doeth good like a medicine: but a broken spirit drieth the bones."]
    
    @IBOutlet weak var btnTimeline: UIButton!
    @IBOutlet weak var TimelineView: UIView!
    
    
    
    // MARK: - Profile Info View
    
    @IBOutlet weak var btnProfileInfo: UIButton!
   
    @IBOutlet weak var ProfileInfoview: UIView!
    @IBOutlet weak var lblUserProfileUsrName,lblProfilAddress,lblProfileName,lblUserProfileEmail,lblProfileBirthDate,lblEmail: UILabel!
    
    @IBOutlet weak var txtFieldAddress,txtFieldUserName,txtFieldUserProfileName,txtFieldEmail: UITextField!
    
    // MARK: - Photo View

    @IBOutlet weak var btnPhotos: UIButton!
    @IBOutlet weak var RecentPhoto: UIView!
    @IBOutlet weak var collectionOfImage: UICollectionView!
    
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        self.view.addSubview(ctb.view)
        
        btnProfileInfo.isSelected = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnProfileInfoPressed(_ sender: Any) {
        
        if btnProfileInfo.isSelected == true {
            btnProfileInfo.isSelected = false
        }
        else {
            
            btnProfileInfo.isSelected = true
            btnTimeline.isSelected = false
            btnPhotos.isSelected = false
            
            ProfileInfoview.isHidden = false
            TimelineView.isHidden = true
            RecentPhoto.isHidden = true
        }
    }
    
    @IBAction func btnTimelinePressed(_ sender: Any) {
        
        if btnTimeline.isSelected == true {
            btnTimeline.isSelected = false
        }
        else {
            
            btnTimeline.isSelected = true
            btnProfileInfo.isSelected = false
            btnPhotos.isSelected = false
            
            ProfileInfoview.isHidden = true
            TimelineView.isHidden = false
            RecentPhoto.isHidden = true
        }
    }
    
    @IBAction func btnPhotosPressed(_ sender: Any) {
        if btnPhotos.isSelected == true {
            btnPhotos.isSelected = false
        }
        else {
            btnProfileInfo.isSelected = false
            btnTimeline.isSelected = false
            btnPhotos.isSelected = true
            
            ProfileInfoview.isHidden = true
            TimelineView.isHidden = true
            RecentPhoto.isHidden = false
            
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //    MARK: - Table view Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDate.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineCell") as! TimelineCell
        
        cell.lblDayName.text = arrDayName[indexPath.row] as?  String
        cell.lblTimelineDate.text = arrDate[indexPath.row] as?  String
        cell.lblUserTimeline.text = arruserTimeline[indexPath.row] as?  String
        cell.lblUserTimeline.adjustsFontSizeToFitWidth = true
        
        return cell;
    }
    
    // MARK: - Collection View Delegate
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Pcell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentImageCell", for: indexPath) as! RecentImageCell
        
        Pcell.imgRecent.image = UIImage(named: "GCblue-ppl.png")
        
        return Pcell
    }
}

class TimelineCell : UITableViewCell {
    
    @IBOutlet weak var lblDayName: UILabel!
    @IBOutlet weak var lblTimelineDate: UILabel!
    @IBOutlet weak var imgLine: UIImageView!
    @IBOutlet weak var lblUserTimeline: UILabel!
    
}

class  RecentImageCell  : UICollectionViewCell {
    
    @IBOutlet weak var imgRecent: UIImageView!
}
