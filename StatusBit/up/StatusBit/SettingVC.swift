//
//  SettingVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/10/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class SettingVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var lblSetting: UILabel!
    @IBOutlet weak var tblSettingMenu: UITableView!

    
    var arrSettingMenuName:NSMutableArray = ["Select Language","Edit Profile","Notifications","Privacy Policy","Terms & Conditions","Notification Sound","Logout"]
    
    var arrSettingMenuImage:NSMutableArray = ["settinglaungage.png","settingedit-profile.png","settingnotifications.png","settingprivecy-policy.png","settingtearms&condition.png","settingnotification.png","Sidelogout.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       bottomBorder(lbl: lblSetting)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bottomBorder(lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  lbl.frame.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    //    MARK: - Table view Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettingMenuName.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingMenuCell") as! SettingMenuCell
        
        cell.imgSettingMenu.image = UIImage(named: arrSettingMenuImage[indexPath.row] as!  String)
        cell.lblSettingMenuName.text = arrSettingMenuName[indexPath.row] as?  String
        
        return cell;
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            //            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
            //            navigationController?.pushViewController(gcvc, animated: true)
            
        }
            
        else if indexPath.row == 1 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 2 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
            navigationController?.pushViewController(gcvc, animated: true)
            
        }
            
        else if indexPath.row == 3 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
            gcvc.page = 1 
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 4 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "OtherDetailPageVC") as! OtherDetailPageVC
            gcvc.page = 1
            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 5 {
            //            let gcvc = storyboard?.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
            //            navigationController?.pushViewController(gcvc, animated: true)
        }
            
        else if indexPath.row == 6 {
            let gcvc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            gcvc.logout = 1
            navigationController?.pushViewController(gcvc, animated: true)
        }
        
        
    }
}

class SettingMenuCell :UITableViewCell {
    @IBOutlet weak var imgSettingMenu: UIImageView!
    @IBOutlet weak var lblSettingMenuName: UILabel!
}
