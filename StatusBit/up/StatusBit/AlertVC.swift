//
//  AlertVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/6/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class AlertVC: UIViewController {

    var ctb = CustomTabBarVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        ctb.btnAlert.isSelected = true
        self.view.addSubview(ctb.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        let controller:SideMenuVc = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuVc") as! SideMenuVc
        controller.view.frame = self.view.bounds;
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }

}
