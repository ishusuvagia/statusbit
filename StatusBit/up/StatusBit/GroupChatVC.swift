//
//  GroupChatVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/4/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class GroupChatVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
// MARK: - Sub View
    
    @IBOutlet weak var SubMenuuView: UIView!
    @IBOutlet weak var btnIM: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnVideoC: UIButton!

    var arrTitle = ["FAMILY MAD CIRCLE","FRIENDS","DOCTOR HEALTHCARE PROFFESSIONAL","COMMUNITY SUPPORT","CARE OF OTHER"]
    
    @IBOutlet weak var tblListPerson: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    
// MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomBorder(select: 0, lbl: lblHeader)
        SubMenuuView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bottomBorder(select:NSInteger, lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.gray.cgColor
        }
        else {
            border.borderColor = UIColor.orange.cgColor
        }
        
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  UIScreen.main.bounds.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSettingPressed(_ sender: Any) {
        let svc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        navigationController?.pushViewController(svc, animated: true)
    }
    
// MARK: - Table View Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListChatCell") as! ListChatCell
        
        bottomBorder(select: 1, lbl: cell.lblTitle)
        cell.lblTitle.text = arrTitle[indexPath.row] as String
        
        cell.collectionOfUser.reloadData()
        
        return cell
    }
    
    // MARK: - Collection View Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Pcell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonListCell", for: indexPath) as! PersonListCell
        Pcell.imgUserProfile.image = UIImage(named: "GCgray-ppl.png")
        Pcell.imgUserState.image = UIImage(named: "GCoffline.png")
        if indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 7 {
            Pcell.imgUserProfile.image = UIImage(named: "GCblue-ppl.png")
             Pcell.imgUserState.image = UIImage(named: "GConline.png")
        }
        
        return Pcell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SubMenuuView.isHidden = false
    }
    
    func clear() {
        btnVideoC.isSelected = false
        btnChat.isSelected = false
        btnIM.isSelected = false
    }
    // MARK: - Sub View Action Method
    
    @IBAction func btnNext(_ sender: Any) {
        clear()
    }
    
    @IBAction func btnPlus(_ sender: Any) {
    }
    
    
    @IBAction func btnVideoConference(_ sender: Any) {
        
        if btnVideoC.isSelected == true {
            btnVideoC.isSelected = false
        }
        else {
            clear()
            btnVideoC.isSelected = true
        }
    }
    
    @IBAction func btnChat(_ sender: Any) {
        if btnChat.isSelected == true {
            btnChat.isSelected = false
        }
        else {
            clear()
            btnChat.isSelected = true
        }
    }
    
    @IBAction func btnIM(_ sender: Any) {
        if btnIM.isSelected == true {
            btnIM.isSelected = false
        }
        else {
            clear()
            btnIM.isSelected = true
        }
    }
    
    @IBAction func btnClose(_ sender: Any) {
        SubMenuuView.isHidden = true
    }
}

class ListChatCell: UITableViewCell {
    
    @IBOutlet weak var collectionOfUser: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
}

class PersonListCell: UICollectionViewCell {
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var imgUserState: UIImageView!
}
