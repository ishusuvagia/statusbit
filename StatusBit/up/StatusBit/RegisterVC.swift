//
//  RegisterVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/3/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import Foundation
import UIKit


class RegisterVC: UIViewController,UITextFieldDelegate,FBSDKLoginButtonDelegate, GIDSignInUIDelegate {
    /**
     Sent to the delegate when the button was used to login.
     - Parameter loginButton: the sender
     - Parameter result: The results of the login
     - Parameter error: The error (if any) from the login
     */
    @IBOutlet weak var btnFBLogin: FBSDKLoginButton!
    
    @IBOutlet weak var btnGoogleSignin: GIDSignInButton!
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("User Logged In")
        
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
                let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                navigationController?.pushViewController(hvc, animated: true)
            }
        }

    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }

    
//    MARK: - Register View
    
    var logout: NSInteger = 0
    
    
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var btnTAndC: UIButton!
    @IBOutlet weak var txtFieldFirstName: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldLastName: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldRegEmail: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldRegPhoneNu: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldRegPsd: VMFloatLabelTextField!
    
//    MARK: - Login View
        
    

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var txtFieldEmail: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldPhoneNumber: VMFloatLabelTextField!
    @IBOutlet weak var txtFieldPassword: VMFloatLabelTextField!
    
    
//    MARK: - IBOutlet
    
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var lblWelComeStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblWelComeStatus.adjustsFontSizeToFitWidth = true
        loginView.isHidden = true
        registerView.isHidden = true
        
//        GIDSignIn.sharedInstance().clientID = "120947692872-d2mvi62q5arq9elop31qvnhb7mmvfhf5.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().uiDelegate = self;
        
        btnFBLogin.readPermissions = ["public_profile", "email", "user_friends"]
        btnFBLogin.delegate = self
        
        drawLine()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if logout == 1 {
            GIDSignIn.sharedInstance().signOut()
            FBSDKLoginManager().logOut()
        }
    }
    
    func bottomBorder(select:NSInteger, txtField:UITextField) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.orange.cgColor
        }
        else {
            border.borderColor = UIColor.lightGray.cgColor
        }
        
        border.frame = CGRect(x: 0, y: txtField.frame.size.height - width, width:  txtField.frame.size.width, height: txtField.frame.size.height)
        
        border.borderWidth = width
        txtField.layer.addSublayer(border)
        txtField.layer.masksToBounds = true
    }
    
    func unSelecteAllBtn() {
        btn1.isSelected = false
        btn2.isSelected = false
        btn3.isSelected = false
    }
    
    func drawLine() {
        bottomBorder(select: 0, txtField: txtFieldEmail)
        bottomBorder(select: 0, txtField: txtFieldPassword)
        bottomBorder(select: 0, txtField: txtFieldPhoneNumber)
        bottomBorder(select: 0, txtField: txtFieldRegEmail)
        bottomBorder(select: 0, txtField: txtFieldRegPsd)
        bottomBorder(select: 0, txtField: txtFieldRegPhoneNu)
        bottomBorder(select: 0, txtField: txtFieldLastName)
        bottomBorder(select: 0, txtField: txtFieldFirstName)
        
    }
    
    //    MARK: - Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.becomeFirstResponder()
        bottomBorder(select: 1, txtField: textField)
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        bottomBorder(select: 0, txtField: textField)
        return true
    }
    
//    MARK: - Google SignIn Method
    
    @IBAction func signupWithGoogle(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signIn()
    }
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//        myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
//    MARK: - Action Method
    
    @IBAction func btn1Pressed(_ sender: Any) {
        if btn1.isSelected == true {
            btn1.isSelected = false
        }
        else {
            unSelecteAllBtn()
            btn1.isSelected = true
        }
    }
    
    @IBAction func btn2Pressed(_ sender: Any) {
        if btn2.isSelected == true {
            btn2.isSelected = false
        }
        else {
            unSelecteAllBtn()
            btn2.isSelected = true
        }
    }
    
    @IBAction func btn3Pressed(_ sender: Any) {
        if btn3.isSelected == true {
            btn3.isSelected = false
        }
        else {
            unSelecteAllBtn()
            btn3.isSelected = true
        }
    }
    @IBAction func loginWithEmail(_ sender: Any) {
        loginView.isHidden = true
        registerView.isHidden = false
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        loginView.isHidden = false
        registerView.isHidden = true
    }
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        navigationController?.pushViewController(hvc, animated: true)
    }
    @IBAction func btnRegisterPressed(_ sender: Any) {
        let hvc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        navigationController?.pushViewController(hvc, animated: true)
    }
    @IBAction func btnTermsCondition(_ sender: Any) {
        if btnTAndC.isSelected == true {
            btnTAndC.isSelected = false
        }
        else {
            btnTAndC.isSelected = true
        }
    }
    
}
