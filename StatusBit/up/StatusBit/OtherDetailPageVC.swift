//
//  OtherDetailPageVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/11/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class OtherDetailPageVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var page : NSInteger = 0
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblTitleHeader: UILabel!
    
    var selectedIndexPath : IndexPath?

//    MARK: - Private Policy View
    
    @IBOutlet weak var PrivatePolicyView: UIView!
    @IBOutlet weak var lblPP: UILabel!
    @IBOutlet weak var lblModifiedPP: UILabel!
    @IBOutlet weak var webViewForPP: UIWebView!
    
    var mutableStrPP = NSMutableAttributedString()
    var mutableStrPPModified = NSMutableAttributedString()
    
//    MARK: - About Us View

    @IBOutlet weak var AboutUsView: UIView!
    
//    MARK: - TC View

    let arrTitle : NSMutableArray = ["FAQ Queries","What is an StatusBit?","Lorem Ipsum Dummy?","Create a POCID Care Network?","I can't remember my login password.Can you send it to me?","I received a negative review.Can it Be removed?","How does the keyword search feature work?","How do i choose a proper category for"]
    @IBOutlet weak var TCView: UIView!
    @IBOutlet weak var tblTC: UITableView!
    
//    MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showPage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showPage() {
        if page == 1 {
            PrivatePolicyView.isHidden = false
            AboutUsView.isHidden = true
            TCView.isHidden = true
            setPP()
        }
        else if page == 2 {
            PrivatePolicyView.isHidden = true
            AboutUsView.isHidden = false
            TCView.isHidden = true
            setPP()
        }
        else if page == 3 {
            PrivatePolicyView.isHidden = true
            AboutUsView.isHidden = true
            TCView.isHidden = false
            setPP()
            lblTitleHeader.text = "Help & FAQ"
        }
    }
    
    func setPP() {
        
        lblTitleHeader.text = "Private Policy"
//        btnEdit.isHidden = true
        
        let str1Attribute = [NSForegroundColorAttributeName:UIColor(red:0.00, green:0.28, blue:1.00, alpha:1.0), NSFontAttributeName:UIFont(name: "AppleSDGothicNeo-Bold", size: 22.0)]
        let str2Attribute = [NSForegroundColorAttributeName:UIColor(red:0.00, green:0.28, blue:1.00, alpha:1.0), NSFontAttributeName:UIFont(name: "AppleSDGothicNeo-Thin", size: 22.0)]
        mutableStrPP = NSMutableAttributedString(string: "StatusBit ", attributes: str1Attribute)
        let partTwo = NSMutableAttributedString(string: "Terms & Conditions", attributes: str2Attribute)
        mutableStrPP.append(partTwo)
        lblPP.attributedText = mutableStrPP
        
        let str3Attribute = [NSForegroundColorAttributeName:UIColor.black, NSFontAttributeName:UIFont(name: "AppleSDGothicNeo-Bold", size: 12.0)]
        let str4Attribute = [NSForegroundColorAttributeName:UIColor.black, NSFontAttributeName:UIFont(name: "AppleSDGothicNeo-Thin", size: 12.0)]
        mutableStrPPModified = NSMutableAttributedString(string: "Last Modified: ", attributes: str3Attribute)
        let partModified = NSMutableAttributedString(string: "April 11, 2017", attributes: str4Attribute)
        mutableStrPPModified.append(partModified)
        lblModifiedPP.attributedText = mutableStrPPModified
    }
    
    func showGeneralNotes() {
        lblTitleHeader.text = "Notes General"
        btnEdit.isHidden = false
    }
    
//    MARK: - Expandable Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 2
        }
        else if section == 2 {
            return 4
        }
        else {
            return 2
        }
    }
    
    internal func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TCCell", for: indexPath) as! TCCell
        
        if indexPath.section == 0 {
            cell.lblTitle.text = arrTitle[indexPath.row] as? String
        }
        else if indexPath.section == 1 {
            cell.lblTitle.text = arrTitle[indexPath.row+2] as? String
        }
        else {
            cell.lblTitle.text = arrTitle[indexPath.row+4] as? String
        }
        
        cell.lblTitle.frame = CGRect(x: cell.lblTitle.frame.origin.x, y: cell.lblTitle.frame.origin.y, width: cell.lblTitle.frame.size.width, height: 40)
        
        
        cell.lblTCDetail.text = "You have been much more to me than just a doctor. You have been my therapist, supporter, friend, well wisher and angel in disguise.Thank you.To study the phenomena of disease without books is to sail an uncharted sea, while to study books without patients is not to go to sea at all.The physician should not treat the disease but the patient who is suffering from it"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let previousIndexPath = selectedIndexPath
        
        if indexPath == selectedIndexPath {
            selectedIndexPath = nil
        } else {
            selectedIndexPath = indexPath
        }
        
        var indexPaths : Array<IndexPath> = []
        
        if let previous = previousIndexPath {
            indexPaths += [previous]
        }
        if let current = selectedIndexPath {
            indexPaths += [current]
        }
        if indexPaths.count > 0 {
            tableView.reloadRows(at: indexPaths, with: UITableViewRowAnimation.automatic)
        }
        
        tblTC.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! TCCell).watchFrameChanges()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! TCCell).ignoreFrameChanges()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        for cell in tblTC.visibleCells as! [TCCell] {
            cell.ignoreFrameChanges()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == selectedIndexPath {
            return TCCell.expandedHeight
        } else {
            return TCCell.defaultHeight
        }
    }

    
//    MARK: - Action Methods
    
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCheckUpdate(_ sender: Any) {
        
    }
    
    @IBAction func btnRateReview(_ sender: Any) {
        
    }
    
    @IBAction func btnShare(_ sender: Any) {
        
    }
    
    @IBAction func btnAboutPP(_ sender: Any) {
        PrivatePolicyView.isHidden = false
        AboutUsView.isHidden = true
    }

    @IBAction func btnAboutTC(_ sender: Any) {
        
    }
}

class TCCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTCDetail: UILabel!
    
    var isObserving = false;
    
    
    
    class var expandedHeight: CGFloat { get { return 116 } }
    class var defaultHeight: CGFloat  { get { return 50  } }
    
    func checkHeight() {
        lblTCDetail.isHidden = (frame.size.height < TCCell.expandedHeight)
    }
    
    func watchFrameChanges() {
        if !isObserving {
            addObserver(self, forKeyPath: "frame", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.initial], context: nil)
            isObserving = true;
        }
    }
    
    func ignoreFrameChanges() {
        if isObserving {
            removeObserver(self, forKeyPath: "frame")
            isObserving = false;
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "frame" {
            checkHeight()
        }
    }

    
}
