//
//  HomeVC.swift
//  StatusBit
//
//  Created by iSquare infoTech on 4/3/17.
//  Copyright © 2017 MitsSoft. All rights reserved.
//

import UIKit

class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var lblMyStatusHeader: UILabel!
    
    @IBOutlet weak var lblMyStatus: UILabel!
    @IBOutlet weak var tblStatus: UITableView!
    
    var ctb = CustomTabBarVC()
    
//    MARK: - Add New sub DCN View
    
    @IBOutlet weak var NewDCNView: UIView!
    @IBOutlet weak var imgUserNewDcn: UIImageView!
    @IBOutlet weak var lblUserNewDCN: UILabel!
    @IBOutlet weak var imgRelationUser: UIImageView!
    @IBOutlet weak var lblRelationUser: UILabel!
    @IBOutlet weak var imgRelatedDCNView: UIImageView!
    @IBOutlet weak var lblRelatedDCNView: UILabel!
    @IBOutlet weak var lblRelatedUserName: UILabel!
    @IBOutlet weak var lblSubDCNStatus: UILabel!
    @IBOutlet weak var lblSubDCNMystatus: UILabel!
    
//    MARK: - Add New DCN View
    
    @IBOutlet weak var txtFieldDCNRelation,txtFieldDCNBirthDate,txtFieldDCNPhone,txtFieldDCNLastName,txtFieldDCNFirstName: VMFloatLabelTextField!
    @IBOutlet weak var addDCNView: UIView!
    
//    MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ctb.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 44, width: UIScreen.main.bounds.size.width, height: 45)
        ctb.btnStatus.isSelected = true
        self.view.addSubview(ctb.view)
        
        addDCNView.isHidden = true
        NewDCNView.isHidden = true
        
        bottomBorder(select: 0, lbl: lblMyStatusHeader)
        bottomBorder(select: 1, lbl: lblMyStatus)
        
        drawLine()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bottomBorder(select:NSInteger, lbl:UILabel) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.darkGray.cgColor
        }
        else {
            border.borderColor = UIColor(red:0.02, green:0.27, blue:1.00, alpha:1.0).cgColor
        }
        
        border.frame = CGRect(x: 0, y: lbl.frame.size.height - width, width:  lbl.frame.size.width, height: lbl.frame.size.height)
        
        border.borderWidth = width
        lbl.layer.addSublayer(border)
        lbl.layer.masksToBounds = true
    }
    
    func bottomBorderTextField (select:NSInteger, txtField:UITextField) {
        let border = CALayer()
        let width = CGFloat(2.0)
        if select == 1 {
            border.borderColor = UIColor.orange.cgColor
        }
        else {
            border.borderColor = UIColor.gray.cgColor
        }
        
        border.frame = CGRect(x: 0, y: txtField.frame.size.height - width, width:  txtField.frame.size.width, height: txtField.frame.size.height)
        
        border.borderWidth = width
        txtField.layer.addSublayer(border)
        txtField.layer.masksToBounds = true
    }
    
    func drawLine() {
       
        view.endEditing(true)
        
        bottomBorderTextField(select: 0, txtField: txtFieldDCNBirthDate)
        bottomBorderTextField(select: 0, txtField: txtFieldDCNRelation)
        bottomBorderTextField(select: 0, txtField: txtFieldDCNPhone)
        bottomBorderTextField(select: 0, txtField: txtFieldDCNLastName)
        bottomBorderTextField(select: 0, txtField: txtFieldDCNFirstName)
        
        txtFieldDCNFirstName.text = ""
        txtFieldDCNLastName.text = ""
        txtFieldDCNBirthDate.text = ""
        txtFieldDCNPhone.text = ""
        txtFieldDCNPhone.text = ""
        
        
    }
    
//    MARK: - Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.becomeFirstResponder()
        bottomBorderTextField(select: 1, txtField: textField)
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        bottomBorderTextField(select: 0, txtField: textField)
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        bottomBorderTextField(select: 0, txtField: textField)
        return true
    }
    
//    MARK: - Table View Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell") as! StatusCell
        
        cell.lblMsgByUser.text = "I was an ordinary patient of ordinary means but your team took care of me as if I was the most important person on earth. Thanks doc." as String
        
        return cell
    }
    
    
//    MARK: - Action Methods    
    
    @IBAction func btnMenu(_ sender: Any) {
        
        let controller:SideMenuVc = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuVc") as! SideMenuVc
        controller.view.frame = self.view.bounds;
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }
    @IBAction func BtnAddNewDcn(_ sender: Any) {
        addDCNView.isHidden = false
    }
    
    @IBAction func btnCloseNewDCN(_ sender: Any) {
        drawLine()
        addDCNView.isHidden = true
    }
    
    @IBAction func btnCancelDCN(_ sender: Any) {
        drawLine()
    }
    
    @IBAction func btnAddDCN(_ sender: Any) {
        
        drawLine()
        addDCNView.isHidden = true
        
        lblRelationUser.text = txtFieldDCNRelation.text
        lblRelatedDCNView.text = txtFieldDCNRelation.text
        lblRelatedUserName.text = txtFieldDCNRelation.text
        
        NewDCNView.isHidden = false
    }
    
    @IBAction func btnSettingPressed(_ sender: Any) {
        let svc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        navigationController?.pushViewController(svc, animated: true)
    }
    
    @IBAction func btnAddNewDcnList(_ sender: Any) {
        
        NewDCNView.isHidden = true
        
//        lblRelationUser.text = txtFieldDCNRelation.text
//        lblRelatedDCNView.text = txtFieldDCNRelation.text
//        lblRelatedUserName.text = txtFieldDCNRelation.text
    }
    
}




class StatusCell:UITableViewCell {
    
    @IBOutlet weak var lblMsgByUser: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgShowDays: UILabel!
    
}
